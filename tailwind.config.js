module.exports = {
  content: ["./src/**/*.{ts,tsx,html}", "public/index.html"],
  theme: {
    extend: {},
    fontFamily: {
      mono: ["Cousine", "ui-monospace", "monospace"],
    },
  },
  plugins: [],
};
